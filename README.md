# Descomplicando o Gitlab

Treinamento Descomplicando o Gitlab criado ao vivo na Twitch

### Day-1

```bash
 - Entendemos o que é o Git
 - Ebtebdemos o que é Working Dir, Index e HEAD
 - Entendemos o que é o GitLab
 - Como criar um grupo no GitLab
 - Como criar um repositório no Git
 - Aprendemos os comandos básicos para manipulação de arquivos e diretórios no Git
 - Como criar uma Branch
 - Como criar um Merge Request
 - Como adicionar um membro no projeto
 - Como fazer merge na Master/Main
 - Como associar um repo local com um repo remoto
 - Como importar um repo do gitHub para o GitLab
 - Mudamos o nome da branch padrão para main
 ```
